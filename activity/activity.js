// Result of using MongoDB Aggregation to count the total number of fruits on sale.

db.fruits.aggregate([
        {
            $match: {onSale: true }
        },
	{ $count: "fruitsOnSale"}

]);

// Result of using MongoDB Aggregation to count the total number of fruits with stock more than 20.

db.fruits.aggregate([
	{
		$match: { stock: {$gte: 20} }
	},
	{
		$group: {
			_id: "$supplier_id",
                   enoughStocks: { $sum: "$stock"},
		}
	},
        {
		$project: { _id: 0}
	}

]);





// Result of using MongoDB Aggregation to get the average price of fruits onSale per supplier.

db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			avg_price: {$avg: "$price"}
		}
	}

]);


// 4.

db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                        price: { $sum: "$price"}
		}
	},
	{
		$project: { totalStocks: 0, _id: 0}
	},
        {
            $sort: { max_price: 1 }
        }

]);

// 5.
db.fruits.aggregate([
	{
		$match: { onSale: true }
	},
	{
		$group: {
			_id: "$supplier_id",
			totalStocks: { $sum: "$stock"},
                        min_price: { $sum: "$price"}
		}
	},
	{
		$project: { totalStocks: 0}
	},
        {
            $sort: { min_price: 1 }
        }

]);
